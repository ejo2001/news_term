FROM python:3.9

WORKDIR /project

COPY ./main.py /project/

COPY ./requirements.txt /project/

COPY ./news_fetch.py /project/

RUN pip install -r /project/requirements.txt

ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]

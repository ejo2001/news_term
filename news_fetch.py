# pylint: disable=line-too-long
# pylint: disable=missing-function-docstring
# pylint: disable=bare-except
# pylint: disable=trailing-whitespace
# pylint: disable=missing-module-docstring

#Imports
import sqlite3
import os
import uuid
from dotenv import load_dotenv
import requests 

#Load .env file 
load_dotenv()

#Declare .env variables
reddit_link = os.getenv('REDDIT_NEWS')
ny_times_link = os.getenv('NY_TIMES')
the_guardian_link = os.getenv('THE_GUARDIAN')
news_api_se_link = os.getenv('NEWS_API_SE')
news_api_us_link = os.getenv('NEWS_API_US')

#Connect to database
Conn = sqlite3.connect('ArticleArchive.db', check_same_thread=False)

#Set up Cursor
Cursor = Conn.cursor()

#Try to set up a database if non exist
try:
    Cursor.execute('''CREATE TABLE Articles (Id INT, Title TEXT, Source TEXT, Desc TEXT, Author TEXT, Link TEXT)''')
except:
    print("Database already exist")


#Fetch news from Reddit
def fetch_reddit():

    #Call the API and set up a variable with the Json data
    red_items = requests.get(reddit_link, headers = {'User-agent': 'your bot 0.1'})
    red_json = red_items.json()

    #Insert the data into the database
    for value in red_json['data']['children']:
        Cursor.execute("INSERT INTO Articles (Id, Title, Source, Desc, Author, Link) VALUES (?, ?, ?, ?, ?, ?)", (str(uuid.uuid4()), str(value['data']['title']), str(value['data']['domain']), str(value['data']['url']), str(value['data']['author_fullname']), str(value['data']['url'])))




#Fetch news from New York Times
def fetch_ny_times():

    #Call the API and set up a variable with the Json data
    time_items = requests.get(ny_times_link)
    time_json = time_items.json()

    #Insert the data into the database
    for value in time_json['results']:
        Cursor.execute("INSERT INTO Articles (Id, Title, Source, Desc, Author, Link) VALUES (?, ?, ?, ?, ?, ?)", (str(uuid.uuid4()), str(value['title']), str(value['url']), str(value['abstract']), str(value['byline']), str(value['url'])))


#Fetch news from The Guardian
def fetch_the_guardian():

    #Call the API and set up a variable with the Json data
    guardian_items = requests.get(the_guardian_link)
    guardian_json = guardian_items.json()

    #Insert the data into the database
    for value in guardian_json['response']['results']:
        Cursor.execute("INSERT INTO Articles (Id, Title, Source, Desc, Author, Link) VALUES (?, ?, ?, NULL, NULL, ?)", (str(uuid.uuid4()), str(value['webTitle']), str(value['webUrl']), str(value['webUrl'])))

#Fetch news from the Swedish news api
def fetch_news_api_se():

    #Call the API and set up a variable with the Json data
    se_items = requests.get(news_api_se_link)
    se_json = se_items.json()

    #Insert the data into the database
    for value in se_json['articles']:
        Cursor.execute("INSERT INTO Articles (Id, Title, Source, Desc, Author, Link) VALUES (?, ?, ?, ?, ?, ?)", (str(uuid.uuid4()), str(value['title']), str(value['url']), str(value['content']), str(value['author']), str(value['url'])))

#Fetch news from the US news api
def fetch_news_api_us():

    #Call the API and set up a variable with the Json data
    us_items = requests.get(news_api_us_link)
    us_json = us_items.json()
    
    #Insert the data into the database
    for value in us_json['articles']:
        Cursor.execute("INSERT INTO Articles (Id, Title, Source, Desc, Author, Link) VALUES (?, ?, ?, ?, ?, ?)", (str(uuid.uuid4()), str(value['title']), str(value['url']), str(value['content']), str(value['author']), str(value['url'])))


#Drop the old table and create a new and update all the values
def update_db():
    Cursor.execute("DROP TABLE Articles")
    Cursor.execute('''CREATE TABLE Articles (Id INT, Title TEXT, Source TEXT, Desc TEXT, Author TEXT, Link TEXT)''')
    fetch_reddit()
    fetch_ny_times()
    fetch_the_guardian()
    fetch_news_api_se()
    fetch_news_api_us()

#Get all articles from the database and return it as a json
def fetch():
    Cursor.execute("SELECT * FROM Articles")
    artics = []
    for i in (Cursor.fetchall()):
        artics.append({"id": str(i[0]), "Title": str(i[1]),"Source": str(i[2]), "Desc": str(i[3]),"Author": str(i[4]), "Link": str(i[5])})
    return {"Articles": artics}

#Initial DB update
update_db()

#Commit changes to database
Conn.commit()

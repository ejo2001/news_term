<h1>News Term (U06)</h1>

<h3>Introduction</h3>

This repository is a school project. The project aims to make it easier to fetch news articles from the terminal. The project runs in docker, which hosts an API server using FastAPI and a database using SQLite3.

<h3>Setup</h3>

To get started, download the project repository using

`git clone https://gitlab.com/ejo2001/news_term.git`

cd into the project

`cd /PATH/TO/PROJECT`

and add the .env file (not bundled with the repository). The .env file should look something like this

```
REDDIT_NEWS=https://www.reddit.com/r/worldnews/.json?count=20 #r/worldnews https://www.reddit.com/r/worldnews/
NY_TIMES=https://api.nytimes.com/svc/topstories/v2/world.json?api-key= #Get your API key at: https://developer.nytimes.com/
THE_GUARDIAN=https://content.guardianapis.com/search?page=2&q=debate&api-key= #Get your API key at: https://open-platform.theguardian.com/
NEWS_API_SE=https://newsapi.org/v2/top-headlines?country=se&apiKey= #Get your API key at: https://newsapi.org/
NEWS_API_US=https://newsapi.org/v2/top-headlines?country=us&apiKey= #Get your API key at: https://newsapi.org/
```

Download and run the docker container

`sudo docker pull registry.gitlab.com/ejo2001/news_term; sudo docker run -it --rm -p 80:80 --env-file ~/news_term/.env --name TestAPI registry.gitlab.com/ejo2001/news_term`

This will download the custom docker image for this project and run it on port 80:80. To call on it, use the endpoints 

`0.0.0.0:80/update`

and

`0.0.0.0:80/fetch`

Update is going to update the database with new articles. Be carefull not to update too often, as the API keys have limited use. 
Fetch will return articles stored in the database. Each article contains an id (uuid), a title, the source, a description, the author, and a link to the article/source. Use the client.py program bundled in this repository to sort this data for easier readability.

<h3>Optional</h3>

To make it easier to run and test the project, I've set up a setup file (setup.sh). Run it with 

`./setup.sh`

to get shortcuts for testing. These shortcuts are:

Run the docker container
`docknews`

Update API database
`clupdate`

Fetch articles from database and present them in a readable format
`clfetch`

These aliases will be saved to the .bashrc file of the logged in user. They should help you run and test the application.

<h3>Credits</h3>

This project was created by Ejo2001

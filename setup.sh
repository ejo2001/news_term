pathvar=$(pwd)
dn=$(cat ~/.bashrc | grep docknews)
clf=$(cat ~/.bashrc | grep clfetch)
clu=$(cat ~/.bashrc | grep clupdate)
if [ -z "$dn" ] && [ -z "$clf" ] && [ -z "$clu" ]; 
then
	read -p 'Docker registry: ' dockerreg
        read -p 'API IP (Default is 0.0.0.0): ' apiip
        echo "alias docknews='sudo docker pull $dockerreg; sudo docker run -it --rm -p 80:80 --env-file ~/news_term/.env --name TestAPI $dockerreg'" >> ~/.bashrc
        echo "alias clfetch='python3 $pathvar/client.py fetch $apiip'" >> ~/.bashrc
        echo "alias clupdate='python3 $pathvar/client.py update $apiip'" >> ~/.bashrc
        echo "Setup done! Added aliases to .bashrc. To fetch articles, use 'clfetch', and to update db, use 'clupdate'. To run the docker container, use 'docknews'."
        exec bash
else
	echo "Already ran setup"
fi
